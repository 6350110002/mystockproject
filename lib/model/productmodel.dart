class ProductModel {
  int? id;
  String productcode;
  String productname;
  String quantity;
  String image;

  ProductModel({
    this.id,
    required this.productcode,
    required this.productname,
    required this.quantity,
    required this.image,

  });

  factory ProductModel.fromMap(Map<String, dynamic> json) =>
      new ProductModel (
        id: json['id'],
        productcode: json['productcode'],
        productname: json['productname'],
        quantity: json['quantity'],
        image: json['image'],
      );

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'productcode': productcode,
      'productname': productname,
      'quantity':  quantity,
      'image': image,
    };
  }
}