
import 'package:flutter/material.dart';
import 'package:producttest/page/about.dart';
import 'package:producttest/page/addproduct.dart';
import 'package:producttest/page/listproduct.dart';
class Homepage extends StatelessWidget {
  const Homepage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
    );
  }
}
    class NavigationDrawer extends StatelessWidget {
        const NavigationDrawer({Key? key}) : super(key: key);

        @override
           Widget build(BuildContext context) => Drawer(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  buildHeader(context),
                  buildMenuItems(context),
                    ],
              ),
          ),
        );
      }

    Widget buildHeader(BuildContext context) => Container(
      color: Colors.blueAccent,
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [
            Text('My Stock',style:
              TextStyle(
                fontSize: 30,color: Colors.white,fontWeight: FontWeight.w400,
              ),
            )
          ],
        ),
      ),
      padding: const EdgeInsets.all(24),
    );
    Widget buildMenuItems(BuildContext context) => Wrap(
      runSpacing: 10,
      children: [
        ListTile(
          leading: const Icon(Icons.add_circle),
          title: const Text('เพิ่มสินค้า'),
          onTap: (){
            Navigator.of(context).push(MaterialPageRoute(builder: (context)=>
             AddProduct()));
          },
        ),
        ListTile(
          leading: const Icon(Icons.list),
          title: const Text('รายการสินค้า'),
          onTap: (){
            Navigator.of(context).push(MaterialPageRoute(builder: (context)=>
                ListProduct(title: 'ListProduct',)));
          },
        ),
        ListTile(
          leading: const Icon(Icons.account_tree_outlined),
          title: const Text('เกี่ยวกับ'),
          onTap: (){
            Navigator.of(context).push(MaterialPageRoute(builder: (context)=>
                About1 ()));
          },
        ),
      ],
    );


