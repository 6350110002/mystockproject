



import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:producttest/database/databasehelp.dart';
import 'package:producttest/model/productmodel.dart';
import 'package:producttest/page/home.dart';


  class AddProduct extends StatefulWidget {
   AddProduct();
  
    @override
    State<AddProduct> createState() => _AddProductState();
  }
  
  class _AddProductState extends State<AddProduct> {
    var productcode = TextEditingController();
    var productname = TextEditingController();
    var quantity = TextEditingController();
    var _image;
    var imagePicker;

    @override
    void initState() {
      super.initState();
      imagePicker = new ImagePicker();
    }
    @override
    Widget build(BuildContext context) {
      return Scaffold(
        drawer: NavigationDrawer(),
        appBar: AppBar(
          centerTitle: true,
          title: const Text('เพิ่มสินค้า'),
          backgroundColor: Colors.blueAccent,
        ),
        body: Container(
        child: ListView(
          children: [
            Padding(padding: const EdgeInsets.fromLTRB(30, 0,30, 0),
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(padding: const EdgeInsets.all(15.15),
                      child: GestureDetector(
                        onTap: () async {
                          final image = await imagePicker.pickImage(
                            source: ImageSource.gallery,
                            imageQuality:50,
                          );
                          setState(() {
                            _image = File(image.path);
                          });
                        },
                        child: Container(
                          width: 200,
                          height: 200,
                          decoration: BoxDecoration(color: Colors.blueAccent),
                            child: _image != null
                                ? Image.file(
                              _image,
                              width: 200.0,
                              height: 200.0,
                              fit: BoxFit.fitHeight,
                            )
                                : Container(
                              decoration: BoxDecoration(
                                  color: Colors.white54),
                              width: 200,
                              height: 200,
                              child: Icon(
                                Icons.camera_alt,
                                color: Colors.black,
                              ),
                            ),
                        ),
                      ),
                    ),
                    buildTextfield('รหัสสินค้า', productcode),
                    buildTextfield('ชื่อสินค้า', productname),
                    buildTextfield('จำนวน', quantity),

                    Padding(padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                      child: Row(
                        mainAxisAlignment:  MainAxisAlignment.spaceAround,
                        children: [
                          buildElevatedButton('เพิ่ม'),
                          buildElevatedButton('ยกเลิก'),
                        ],
                      ),
                    ),

                  ],
                ),
              ),
            )
          ],
        ),
      )
      );
    }
    ElevatedButton buildElevatedButton(String title) {
      return ElevatedButton(
        onPressed: () async {

          if (title == 'เพิ่ม') {
            await DatabaseHelp.instance.add(
              ProductModel(
                  productcode: productcode.text,
                  productname: productname.text,
                  quantity:  quantity.text,
                  image: _image.path),
            );
            setState(() {
              //firstname.clear();
              //selectedId = null;
            });

            Navigator.pop(context);
          }else Navigator.pop(context);
        },
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(20)),
            fixedSize: Size(120, 50),
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            textStyle: const TextStyle(fontSize: 20, ),
            primary: Colors.blueAccent,
        ),

        child: Text(
          title,
        ),
      );
    }
    Padding buildTextfield(String title, final ctrl) {
      return Padding(
        padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
        child: TextField(
          controller: ctrl,
          style: TextStyle(
            fontSize: 20,
          ),
          decoration: InputDecoration(
            hintText: title,
            filled: true,
            fillColor: Colors.white,
            border: OutlineInputBorder(),
          ),
        ),
      );
    }
  }
  

