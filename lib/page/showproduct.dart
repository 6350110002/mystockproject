
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:producttest/database/databasehelp.dart';
import 'package:producttest/model/productmodel.dart';
import 'package:producttest/page/home.dart';

class ShowProduct extends StatelessWidget {
  final id;
  ShowProduct({Key? key, required this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('รายละเอียดสินค้า'),
        backgroundColor: Colors.blueAccent,
      ),
      drawer: NavigationDrawer(),
      body: Center(
        child: FutureBuilder<List<ProductModel>>(
            future: DatabaseHelp.instance.getProduct2(this.id),
            builder: (BuildContext context,
                AsyncSnapshot<List<ProductModel>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('กำลังโหลด...'));
              }
              return snapshot.data!.isEmpty
                  ? Center(child: Text('ไม่มีสินค้าในรายการ.'))
                  : ListView(
                children: snapshot.data!.map((product) {
                  return Center(
                    child: Column(
                      children: [
                        Container(margin: EdgeInsets.only(top: 90),
                          child: CircleAvatar(
                            backgroundImage: FileImage(File(product.image)),
                            radius: 100,
                          ),
                        ),
                        SizedBox(
                          height: 40,
                        ),
                        Text('รหัสสินค้า : ${product.productcode}',style: TextStyle(fontSize: 20)),
                        SizedBox(
                          height: 10,
                        ),
                        Text('ชื่อสินค้า: ${product.productname}',style: TextStyle(fontSize: 20)),
                        SizedBox(
                          height: 10,
                        ),
                        Text('จำนวน: ${product.quantity}',style: TextStyle(fontSize: 20)),
                      ],
                    ),
                  );
                }).toList(),
              );
            }),
      ),
    );
  }
}
