

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:producttest/database/databasehelp.dart';
import 'package:producttest/model/productmodel.dart';
import 'package:producttest/page/editproduct.dart';
import 'package:producttest/page/home.dart';
import 'package:producttest/page/showproduct.dart';


class ListProduct extends StatefulWidget {
  const ListProduct({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<ListProduct> createState() => _ListProductState();
}

class _ListProductState extends State<ListProduct> {

  int? selectedId;

  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('รายการสินค้า' ),
        backgroundColor: Colors.blueAccent,
      ),
      drawer: const NavigationDrawer(),
      body: Center(
        child: FutureBuilder<List<ProductModel>>(
            future: DatabaseHelp.instance.getProduct(),
            builder: (BuildContext context,
                AsyncSnapshot<List<ProductModel>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('กำลังโหลด...'));
              }
              return snapshot.data!.isEmpty
                  ? Center(child: Text('ไม่มีสินค้าในรายการ'))
                  : ListView(
                children: snapshot.data!.map((grocery) {
                  return Center(
                    child: Card(
                      color: selectedId == grocery.id
                          ? Colors.white70
                          : Colors.white,
                      child: ListTile(
                        title: Text('${grocery. productcode} ${grocery.productname}'),
                        subtitle: Text(grocery.quantity),
                        leading: CircleAvatar(backgroundImage: FileImage(File(grocery.image))),
                        trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children:  <Widget>[
                            new IconButton(
                              padding: EdgeInsets.all(0),
                              icon: Icon(Icons.unarchive_outlined),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => EditProduct(grocery)),
                                ).then((value){
                                  setState(() {
                                  });
                                });
                              },
                            ),
                            new IconButton(
                              padding: EdgeInsets.all(0),
                              icon: Icon(Icons.delete_forever),
                              onPressed: () {
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title: new Text('คุณต้องการลบสินค้านี้หรือไม่?'),
                                      actions: [
                                        new TextButton(
                                          onPressed: () {
                                            DatabaseHelp.instance.remove(grocery.id!);
                                            setState(() {
                                              Navigator.of(context).pop();
                                            });
                                          },
                                          child: new Text("ตกลง"),
                                        ),
                                        Visibility(
                                          visible: true,
                                          child: new TextButton(
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                            child: new Text("ยกเลิก"),
                                          ),
                                        ),
                                      ],
                                    );
                                  },
                                );

                              },
                            ),
                          ],
                        ),
                        onTap: () {
                          var productid = grocery.id;
                          Navigator.push(context, MaterialPageRoute(builder: (context) => ShowProduct(id: productid)));

                          setState(() {
                            print(grocery.image);
                            if (selectedId == null) {
                              selectedId = grocery.id;
                            } else {
                              selectedId = null;
                            }
                          });
                        },
                        onLongPress: () {
                          setState(() {
                            DatabaseHelp.instance.remove(grocery.id!);
                          });
                        },
                      ),
                    ),
                  );
                }).toList(),
              );
            }),
      ),
    );
  }
}