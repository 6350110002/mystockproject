


import 'package:flutter/material.dart';
import 'package:producttest/page/home.dart';

class About1 extends StatelessWidget {
  const About1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('ผู้จัดทำ' ),
        backgroundColor: Colors.blueAccent,
      ),
      drawer: const NavigationDrawer(),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(60,20, 50, 0),
            child: CircleAvatar( backgroundImage: AssetImage('assets/buck.jpg'),radius: 100),
          ),
          Center(child: Text('นาย กิตติศักดิ์ สุทธิกิตติพงศ์ 6350110001',style: TextStyle(fontSize: 20),)),
          Padding(
            padding: const EdgeInsets.fromLTRB(60,20, 50, 0),
            child: CircleAvatar( backgroundImage: AssetImage('assets/boy.jpg'),radius: 100),
          ),
          Center(child: Text('นาย ชญานนท์ หมวดสง 6350110002',style: TextStyle(fontSize: 20),)),
        ],
      ),
    );
  }
}
