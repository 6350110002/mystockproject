import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:producttest/database/databasehelp.dart';
import 'package:producttest/model/productmodel.dart';

import 'dart:developer' as developer;

import 'package:producttest/page/home.dart';

class EditProduct extends StatefulWidget {
  ProductModel model;

  EditProduct(this.model);

  @override
  _EditProductState createState() => _EditProductState(model);
}

class _EditProductState extends State<EditProduct> {
  var productcode = TextEditingController();
  var productname = TextEditingController();
  var quantity = TextEditingController();

  ProductModel model;
  int? selectedId;

  var _image;
  var imagePicker;
  _EditProductState(this.model);

  @override
  void initState() {
    super.initState();
    imagePicker = new ImagePicker();
    this.selectedId = this.model.id;
    productcode  = TextEditingController()..text = this.model.productcode;
    productname = TextEditingController()..text = this.model. productname;
    quantity = TextEditingController()..text = this.model.quantity;
    _image = this.model.image;

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('อัพเดทสินค้า'),
        backgroundColor: Colors.blueAccent,
      ),
      drawer: NavigationDrawer(),
      body: Container(
        color: Colors.white,
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.15),
                      child: GestureDetector(
                        onTap: () async {
                          final image = await imagePicker.pickImage(
                              source: ImageSource.gallery,
                              imageQuality: 50,
                              preferredCameraDevice: CameraDevice.front);

                          setState(() {
                            _image = image.path;
                          });
                        },
                        child: Container(
                          width: 200,
                          height: 200,
                          decoration:
                          BoxDecoration(color: Colors.blueAccent),
                          child: _image != null
                              ? Image.file(
                            File(_image),
                            width: 200.0,
                            height: 200.0,
                            fit: BoxFit.fitHeight,
                          )
                              : Container(
                            decoration: BoxDecoration(
                                color: Colors.blueAccent),
                            width: 200,
                            height: 200,
                            child: Icon(
                              Icons.camera_alt,
                              color: Colors.grey[800],
                            ),
                          ),
                        ),
                      ),
                    ),
                    buildTextfield("รหัสสินค้า", productcode),
                    buildTextfield("ชื่อสินค้า", productname),
                    buildTextfield("จำนวน", quantity),

                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          buildElevatedButton('อัปเดต'),
                          buildElevatedButton('ยกเลิก'),
                        ],
                      ),
                    ),
                  ],

                ),

              ),
            ),
          ],
        ),
      ),
    );
  }

  ElevatedButton buildElevatedButton(String title) {
    return ElevatedButton(
      onPressed: () async {
        if (title == 'อัปเดต') {
          await DatabaseHelp.instance.update(
            ProductModel(
                id: selectedId,
                productcode:  productcode.text,
                productname: productname.text,
                quantity:  quantity.text,
                image: _image),
          );
          setState(() {
            //firstname.clear();
            selectedId = null;
          });
          Navigator.pop(context);
        }else Navigator.pop(context);
      }
      ,
      style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(20)),
          fixedSize: Size(120, 50),
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          textStyle:
          const TextStyle(fontSize: 20, ),
          primary: Colors.blueAccent),
      child: Text(
        title,
      ),
    );
  }

  Padding buildTextfield(String title, final ctrl) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
      child: TextField(
        controller: ctrl,
        style: TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          hintText: title,
          filled: true,
          fillColor: Colors.white,
          border: OutlineInputBorder(),
        ),
      ),
    );
  }

}
