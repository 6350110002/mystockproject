

import 'package:flutter/material.dart';
import 'package:producttest/page/home.dart';


class GetStart {
  Color primary=Color(0xff005400);
  Color darkcolor=Color(0xff39821a);
  Color lightColor=Color(0xff6bb249);

  TextStyle darkStyle()=>TextStyle(color: darkcolor);

  Widget showLogo() =>Image(image: AssetImage('assets/home.png'),
  );
  SafeArea buildBackground(double screenWidth,double screenHeight) {
    return SafeArea(
      child: Container(
        child: Stack(
          children: [
            Positioned(
                top: 0,
                left: 0,
                child: Image.asset('assets/top2.png')),
            Positioned(
                top: 0,
                left: 0,
                child: Image.asset('assets/top1.png')),
            Positioned(
                bottom: 0,
                left: 0,
                child: Image.asset('assets/bottom1.png')),
            Positioned(
                bottom: 0,
                left: 0,
                child: Image.asset('assets/bottom2.png')),

          ],
        ),
      ),
    );
  }

  GetStart();
}