import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:producttest/model/productmodel.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelp {
  DatabaseHelp._privateConstructor();
  static final DatabaseHelp instance = DatabaseHelp._privateConstructor();

  static Database? _database;
  Future<Database> get database async => _database ??= await _initDatabase();

  Future<Database> _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'db_product.db');
    return await openDatabase(
      path,
      version: 1,
      onCreate: _onCreate,
    );
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE  (productDB
          id INTEGER PRIMARY KEY,
          productcode TEXT,
          productname TEXT,
          quantity TEXT,
          image TEXT
      )
      ''');
  }

  Future<List<ProductModel>> getProduct() async {
    Database db = await instance.database;
    var product = await db.query('productDB', orderBy: 'productcode');
    List<ProductModel> groceryList = product.isNotEmpty
        ? product.map((c) => ProductModel.fromMap(c)).toList()
        : [];
    return groceryList;
  }

  Future<List<ProductModel>> getProduct2(int id) async{
    Database db = await instance.database;
    var product = await db.query('productDB', where: 'id = ?', whereArgs: [id]);
    List<ProductModel> productList = product.isNotEmpty
        ? product.map((c) => ProductModel.fromMap(c)).toList()
        : [];
    return productList;
  }

  Future<int> add(ProductModel product) async {
    Database db = await instance.database;
    return await db.insert('productDB', product.toMap());
  }

  Future<int> remove(int id) async {
    Database db = await instance.database;
    return await db.delete('productDB', where: 'id = ?', whereArgs: [id]);
  }

  Future<int> update(ProductModel product) async {
    Database db = await instance.database;
    return await db.update('productDB', product.toMap(),
        where: "id = ?", whereArgs: [product.id]);
  }
}