


import 'package:flutter/material.dart';
import 'package:producttest/getstart/getstart.dart';
import 'package:producttest/page/home.dart';
import 'package:producttest/page/listproduct.dart';
import 'package:producttest/router.dart';

void main() {
  runApp(const MyApp());
}

String intRoute='/authen';


class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        routes: map,
        initialRoute: intRoute,
    );

  }
}


