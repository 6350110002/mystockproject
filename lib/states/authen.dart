import 'package:flutter/material.dart';
import 'package:producttest/getstart/getstart.dart';
import 'package:producttest/page/addproduct.dart';

class Authen extends StatefulWidget {
  @override
  State<Authen> createState() => _AuthenState();
}

class _AuthenState extends State<Authen> {
  late double screenWidth, screenHeight;
  late bool redEye = true;


  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;
    screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            GetStart().buildBackground(screenWidth, screenHeight),
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Positioned(
                    top: 50,
                    left: 1,
                    child: buildLogo(),
                  ),
                  buildUser(),
                  buildPassword(),
                  buildLogin(),

                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Container buildLogin() {
    return Container(
      margin: EdgeInsets.only(top: 5),
      width: screenWidth * 0.6,
      child: ElevatedButton(
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context){
            return AddProduct();
          },));
        },
        child: Text('ลงชื่อ'),
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(20)),),
      ),
    );
  }

  Container buildUser() {
    return Container(
      margin: EdgeInsets.only(top: 16),
      width: screenWidth * 0.6,
      child: TextField(
        decoration: InputDecoration(
          prefixIcon: Icon(
            Icons.perm_identity,
            color: GetStart().darkcolor,
          ),
          labelStyle: GetStart().darkStyle(),
          labelText: 'ชื่อผู้ใช้ :',
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(color: GetStart().darkcolor)),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: GetStart().lightColor)),
        ),
      ),
    );
  }

  Container buildPassword() {
    return Container(
      margin: EdgeInsets.only(top: 16),
      width: screenWidth * 0.6,
      child: TextField(
        obscureText: redEye,
        decoration: InputDecoration(
          suffixIcon: IconButton(
            icon: Icon(
              redEye
                  ? Icons.remove_red_eye_outlined
                  : Icons.remove_red_eye_sharp,
              color: GetStart().darkcolor,
            ),
            onPressed: () {
              setState(() {
                redEye = !redEye;
              });
            },
          ),
          prefixIcon: Icon(
            Icons.lock_open_outlined,
            color: GetStart().darkcolor,
          ),
          labelStyle: GetStart().darkStyle(),
          labelText: 'รหัสผ่าน:',
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide(color: GetStart().darkcolor)),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: GetStart().lightColor)),
        ),
      ),
    );
  }

  Container buildLogo() {
    return Container(width: screenWidth * 0.6, child: GetStart().showLogo());
  }
}
